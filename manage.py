from sys import exit
from os import rename, remove, listdir
from os.path import exists, basename
from shutil import move
import click
from weasyprint import HTML

from minsplinter import MinSplinter
from scraper_ocpinfo import config, init_jinja_env, print_document, Pharmacist, Workplace, PharmacistList, \
	ExcelWriter, ExcelReader


def generate_files(env, location, file_type):
	th = '/'.join([config['INSTANCE_PATH'], 'temp.html'])
	template = env.get_template('{0}.html'.format(file_type))

	er = ExcelReader('/'.join([config['INSTANCE_PATH'], config['EXCEL_OUTPUT_FILENAME']]), location)
	er.load_file()

	for ld in er.letter_data():
		path, generated = er.is_generated(file_type, ld['registration_number'])
		if generated:
			continue

		content = template.render(**ld)
		with open(th, 'w') as fp:
			fp.write(content.encode('utf8'))

		HTML(th).write_pdf(path)


def get_files(file_type):
	rft = '-' + file_type + '.pdf'
	files = listdir('/'.join([config['INSTANCE_PATH'], config['PDF_FOLDER']]))
	for f in files:
		i = f.find(rft)
		if i != -1:
			reg_code = f[0:i]
			yield reg_code, '/'.join([config['INSTANCE_PATH'], config['PDF_FOLDER'], f])


@click.group()
def cli():
	pass


@click.command('run-scraper', help="Run scraper.")
@click.option('--location', default=None, help="The area to search in.")
def run_scraper(location):
	b_list = MinSplinter(config['NINSPLINTER_CONFIG'])
	b_pobj = MinSplinter(config['NINSPLINTER_CONFIG'])
	pl = PharmacistList(location, b_list)
	search_ok = pl.trigger_search()

	if not search_ok:
		print 'No pharmacists found for location: {0}'.format(location)
		return 0

	page = 1
	records_to_xlsx = 0
	while page is not None:
		print '== {0}/{1} =='.format(page, pl.pages)

		for l in pl.get_pharmacist_link():
			po = Pharmacist(l, spobj=b_pobj)
			if not po.is_owner():
				continue

			po.fetch_data()
			if not po.is_close_to_retirement():
				continue

			# Pharmacist matches the criteria, proceed to workplaces.
			workplaces = po.get_workplaces()
			for w in workplaces:
				wo = Workplace(w, spobj=b_pobj)

				if not wo.is_accredited():
					continue

				wo.fetch_data()

				# Save data to file.
				ew = ExcelWriter(pl, po, wo)
				ew.write()
				ew.save()
				records_to_xlsx += 1
		page = pl.get_page()
	b_list.quit()
	b_pobj.quit()
	print 'Saved {0} records.'.format(records_to_xlsx)


@click.command('sort-data', help="Sort data in Excel file.")
def sort_data():
	ew = ExcelWriter()
	if ew.sort():
		cp = '/'.join([config['INSTANCE_PATH'], config['EXCEL_SORTED_FILENAME']])
		np = '/'.join([config['INSTANCE_PATH'], config['EXCEL_OUTPUT_FILENAME']])
		if exists(np):
			remove(np)
		rename(cp, np)
	else:
		print 'Sorting failed.'
		exit(1)


@click.command('generate-letters', help="Generate letters based on data in Excel file and templates.")
@click.option('--location', help="Specify location to generate letters for.")
def generate_letters(location):
	env = init_jinja_env()
	generate_files(env, location, 'letter')

	# Dump location so we can read it inside generate_labels function.
	with open('/'.join([config['INSTANCE_PATH'], config['LAST_LOCATION_FILENAME']]), 'w') as fp:
		fp.write(location)


@click.command('generate-labels', help="Generate labels based on letters in pdf dir.")
def generate_labels():
	env = init_jinja_env()
	with open('/'.join([config['INSTANCE_PATH'], config['LAST_LOCATION_FILENAME']]), 'r') as fp:
		location = fp.read()
	generate_files(env, location, 'label')


@click.command('print-letters', help="Print letters located in PDF folder to default printer.")
def print_letters():
	with open('/'.join([config['INSTANCE_PATH'], config['PRINT_ORDER_FILENAME']]), 'w') as fp:
		for reg_code, f in get_files('letter'):
			filename = basename(f)
			af = '/'.join([config['INSTANCE_PATH'], config['PDF_ARCHIVE_FOLDER'], filename])
			
			# If letter to pharmacist with registration number was never printed before print it now.
			if not exists(af):
				print_document(f)
				fp.write(reg_code + '\n')
				move(f, af)


@click.command('print-labels', help="Print labels located in PDF folder to default printer.")
@click.option('--count', default=-1, help="Specify how many labels gets printed.")
def print_labels(count):
	icount = int(count)
	with open('/'.join([config['INSTANCE_PATH'], config['PRINT_ORDER_FILENAME']]), 'r') as fp:
		i = 0
		l = fp.readline()
		l = l[0:-1]
		while len(l) and icount == -1 or i < icount:
			filename = config['LABEL_FILENAME_TEMPLATE'].format(registration_number=l)
			f = '/'.join([config['INSTANCE_PATH'], config['PDF_FOLDER'], filename])
			af = '/'.join([config['INSTANCE_PATH'], config['PDF_ARCHIVE_FOLDER'], filename])
			
			# Print if label file exists and it was never printed before (does not exist in archive directory).
			if exists(f) and not exists(af):
				print_document(f)
				move(f, af)
				i += 1
			l = fp.readline()
			l = l[0:-1]


cli.add_command(run_scraper)
cli.add_command(sort_data)
cli.add_command(generate_letters)
cli.add_command(generate_labels)
cli.add_command(print_letters)
cli.add_command(print_labels)

if __name__ == '__main__':
	config.check_env()
	cli()
