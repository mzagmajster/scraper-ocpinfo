from inspect import getmembers
from os.path import exists
from os import mkdir


class ScriptConfig(dict):
	def __init__(self, config_module):
		c = list()
		opt_list = getmembers(config_module)
		for o in opt_list:
			if o[0].isupper():
				c.append(o)

		super(ScriptConfig, self).__init__(c)

	def check_env(self):
		# config.py
		p = '/'.join([self['INSTANCE_PATH'], 'config.py'])
		if not exists(p):
			fp = open(p, 'w')
			fp.writelines([
				'from scraper_oscpinfo.config import *',
				'# Initialize variables from config package here and their values will be overwritten.'
			])
			fp.close()

		# Create required folders.
		l = [self['TEMPLATES_FOLDER'], self['PDF_FOLDER'], self['PDF_ARCHIVE_FOLDER']]
		for e in l:
			p = '/'.join([self['INSTANCE_PATH'], e])
			if not exists(p):
				mkdir(p)

