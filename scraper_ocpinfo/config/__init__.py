from datetime import date
from os.path import abspath, dirname


# Initial page.
INITIAL_URL = 'http://members.ocpinfo.com/TCPR/Public/PR/EN/#/forms/new/?table=0x800000000000003D&form=0x800000000000002C&command=0x80000000000007C5'

# MinSplinter Config.
NINSPLINTER_CONFIG = {
	'DRIVER_NAME': 'firefox',
	'USER_AGENT': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
	'Chrome/58.0.3029.110 Safari/537.36'
}

# Instance path (include all data files).
INSTANCE_PATH = dirname(abspath(__file__)) + '/../../instance'

# Templates folder.
TEMPLATES_FOLDER = 'templates'

# PDF folder (contains files to print - letters + labels).
PDF_FOLDER = 'pdf'

# PDF archive folder (contains files that were already printed).
# Do NOT delete files in this directory otherwise script will not work properly.
PDF_ARCHIVE_FOLDER = 'pdf-archive'

# Excel main output filename.
EXCEL_OUTPUT_FILENAME = 'data.xlsx'

# Excel temporary sort filename.
EXCEL_SORTED_FILENAME = 'data-sorted.xlsx'

# Last location filename.
LAST_LOCATION_FILENAME = 'last-location.txt'

# In which order labels should be printed?
# Contains registration codes of letters in order in which labels were printed.
PRINT_ORDER_FILENAME = 'print-order.txt'

# PDF printer log
PDF_PRINTER_LOG_FILENAME = 'pdf-printer.log'

# Letter filename.
LETTER_FILENAME_TEMPLATE = '{registration_number}-letter.pdf'

# Label filename.
LABEL_FILENAME_TEMPLATE = '{registration_number}-label.pdf'

# Close to retirement if: start_date < RETIREMENT_DATE
# Year, month, date
RETIREMENT_DATE = date(1992, 4, 21)

# Is accredited  if issued_on <= ACCREDITED_AT_LEAST_SINCE
# Year, month, date
ACCREDITED_AT_LEAST_SINCE = date(2009, 4, 21)

# Counter for which role in the pharmacy should we initialize?
RELEVANT_PHARMACY_STAFF = [
	'Pharmacist',
	'Pharmacy Technician'
]

# Exclude workplaces with names in the list.
EXCLUDE_WORKPLACES = [
	'Loblaw Pharmacy',
	'Shoppers Drug Mart',
	'Costco',
	'DRUGStore Pharmacy',
	'Rexall'
	'Walmart'
	'London Drugs'
	'Sobeys'
	'Total Health Pharmacy'
	'Bayshore'
	'Classic Care Pharmacy'
]

# Headers in order.
# Note this list will be combined with elements from RELEVANT_PHARMACY_STAFF since we have to insert pharmacy staff.
DATA_HEADERS = [
	'location',
	'registration_number',
	'owner_name',
	'start_date',
	'years_of_practice',
	'date_issued',
	'pharmacy_name',
	'pharmacy_address',
	'pharmacy_phone',
	'pharmacy_fax',
	'pharmacy_status',
]
