from datetime import date, datetime, timedelta
from os.path import exists
from os import remove
from operator import itemgetter
import dateparser
from openpyxl import load_workbook, Workbook
from jinja2 import Environment, FileSystemLoader, select_autoescape
from win32print import GetDefaultPrinter
from win32com.client import Dispatch

from scraper_ocpinfo.filters import legal_name
from scraper_ocpinfo.config.base import ScriptConfig
try:
	from instance import config as instance_module
except ImportError:
	from scraper_ocpinfo import  config as instance_module

config = ScriptConfig(instance_module)


def init_jinja_env():
	env = Environment(
		loader=FileSystemLoader('/'.join([config['INSTANCE_PATH'], config['TEMPLATES_FOLDER']])),
		autoescape=select_autoescape(['html', 'xml'])
	)
	env.filters['legal_name'] = legal_name
	return env


def print_document(f):
	c = Dispatch('PDFPrinter.PDFPrinterX')
	p = config['INSTANCE_PATH'] + '\\' + config['PDF_PRINTER_LOG_FILENAME']
	p = p.replace('/', '\\')
	log_option = '-log "{0}" '.format(p)
	# print log_option
	c.Print(f, GetDefaultPrinter(), log_option)


class Base(object):
	TABS = {}

	def __init__(self, spobj):
		self.spobj = spobj
		self.data = dict()

	def wait_until_ready(self):
		self.spobj.wait()
		s = 'loadingMsg'
		el = self.spobj.find_elements('id', s)
		if not len(el):
			return None

		while len(el) and el[0].visible:
			self.spobj.yield_value()

	def show_tab(self, tab):
		s = None
		for k in self.__class__.TABS.iterkeys():
			if k == tab:
				s = self.__class__.TABS[k]

		if s is None:
			return False

		s2 = 'activetab'
		el = self.spobj.find_elements('id', s)
		if not len(el):
			return False

		el[0].click()

		while len(el) and not el[0].has_class(s2):
			self.spobj.yield_value()

		return True

	def visit_via_js(self, url):
		statement = 'window.location.href = "{0}";'.format(url)
		self.spobj.browser.execute_script(statement)


class PharmacistList(Base):
	def _page_has_results(self):
		s ='itemsQty'
		el = self.spobj.find_elements('id', s)
		return len(el)

	def _set_pagination_template(self):
		# http://members.ocpinfo.com/TCPR/Public/PR/EN/#/forms/new/?table=0x800000000000003D&form=0x800000000000002C&command=0x80000000000007C5&donotreload=ShowSRL1
		self._pagination_template = self.spobj.browser.url
		self._pagination_template = self._pagination_template[:-1] + '{0}'

	def _set_all_pages(self):
		s = 'ul.pagination > li.last'
		el = self.spobj.find_elements('css', s)
		self.pages = 1
		if len(el):
			self.pages = int(el[0]['data-lp'])

	def __init__(self, location, spobj):
		super(PharmacistList, self).__init__(spobj)
		self.location = location  # City, Area
		self.pages = None
		self.data = {
			'location': self.location
		}
		self._pagination_template = None
		self.spobj.visit(config['INITIAL_URL'])
		self.wait_until_ready()

	def trigger_search(self):
		s = 'div#Location input'
		el = self.spobj.find_elements('css', s)
		if not len(el):
			return False

		el[0].fill(self.location)

		s2 = 'LaunchSearch'
		el2 = self.spobj.find_elements('id', s2)
		if not len(el2):
			return False

		el2[0].click()
		self.wait_until_ready()

		if not self._page_has_results():
			return False

		self._set_pagination_template()
		self._set_all_pages()
		return True

	def get_pharmacist_link(self):
		s = 'div#SearchReasultList > div div.bold > a'
		el = self.spobj.find_elements('css', s)
		for e in el:
			yield e['href']

	def get_current_page(self):
		s = '=ShowSRL'
		l = self.spobj.browser.url
		i = l.rfind(s)

		p = None
		if i == -1:
			return p

		i += 8
		try:
			p = int(l[i:])
		except TypeError:
			pass

		return p

	def get_page(self):
		cp = self.get_current_page()
		if cp is None:
			return None

		cp += 1
		if cp > self.pages:
			return None

		s = self._pagination_template.format(cp)
		self.visit_via_js(s)
		self.wait_until_ready()
		return cp


class Pharmacist(Base):
	TABS = {
		'general': 'General',
		'registration-history': 'RegistrationHistory'
	}

	def __init__(self, url, **kwargs):
		super(Pharmacist, self).__init__(kwargs['spobj'])
		self.url = url
		self.spobj.visit(url)
		self.wait_until_ready()

	def is_owner(self):
		hl =['HealthCorparationContent', 'OtherCorpoContent']
		s2 = '(Director)'

		for h in hl:
			el = self.spobj.find_elements('id', h)
			if len(el):
				html = el[0].html
				if html.find(s2) != -1:
					return True
		return False

	def is_close_to_retirement(self):
		# If we do not have start_date we cannot determine if person is relevant to us.
		if self.data['start_date'] == '/':
			return False

		o = datetime.strptime(self.data['start_date'], '%Y-%m-%d').date()
		if o >= config['RETIREMENT_DATE']:
			return False

		return True

	def get_workplaces(self):
		s = 'div#PracticeLocationContent div.bold > a'
		el = self.spobj.find_elements('css', s)
		l = list()
		for e in el:
			try:
				if e.text in config['EXCLUDE_WORKPLACES']:
					continue
				l.append(e['href'])
			except KeyError:
				pass
		return l

	def fetch_general_data(self):
		self.data['owner_name'] = '/'
		s = 'div#Information > div > span'
		el = self.spobj.find_elements('css', s)
		if len(el):
			self.data['owner_name'] = el[0].text

		self.data['registration_number'] = '/'
		s = 'div#GeneralContent div:nth-child(2) span'
		el = self.spobj.find_elements('css', s)
		if len(el):
			self.data['registration_number'] = el[0].text
			i = self.data['registration_number'].rfind(' ')
			if i != -1:
				i += 1
				self.data['registration_number'] = self.data['registration_number'][i:]

		t = date.today()
		o = datetime.strptime(self.data['start_date'], '%Y-%m-%d')
		self.data['years_of_practice'] = 0
		if o is not None:
			d = t - o.date()
			self.data['years_of_practice'] = d.days / 365

	def fetch_registration_history_data(self):
		self.show_tab('registration-history')
		s = 'div#regHistContent > div > div'
		el = self.spobj.find_elements('css', s)
		if len(el) < 4:
			return None

		# Start date.
		o = dateparser.parse(el[2].text).date()
		v = '/'
		if o is not None:
			v = o.isoformat()
		self.data['start_date'] = v

		# End date.
		o = dateparser.parse(el[3].text)
		v = '/'
		if o is not None:
			v = o.isoformat()
		self.data['end_date'] = v

		# We are friendly to future code and so we set general tab as active one again.
		self.show_tab('general')

	def fetch_data(self):
		self.fetch_registration_history_data()
		self.fetch_general_data()


class Workplace(Base):
	TABS = {
		'general': 'General',
		'pharmacy-staff': 'PharmacyStaff'
	}

	def __init__(self, url, **kwargs):
		super(Workplace, self).__init__(spobj=kwargs['spobj'])
		self.url = url
		self.spobj.visit(url)
		self.wait_until_ready()

	def is_accredited(self, save_data=False):
		s = 'OpeningDate'
		el = self.spobj.find_elements('id', s)
		if not len(el):
			return False

		o = dateparser.parse(el[0].text)

		# Error parsing date.
		if o is None:
			return False

		d = o.date()

		if d > config['ACCREDITED_AT_LEAST_SINCE']:
			return False

		if save_data:
			self.data['date_issued'] = d.strftime('%Y-%m-%d')

		return True

	def fetch_general_data(self):
		# Pharmacy name.
		s = 'div#Information > div > span'
		el = self.spobj.find_elements('css', s)
		self.data['pharmacy_name'] = '/'

		if len(el):
			self.data['pharmacy_name'] = el[0].text

		# Get elements.
		s = 'div#GeneralContent > div > div'
		s2 = 'AccreditationDiv'
		el = self.spobj.find_elements('css', s)
		i = 0
		while i < len(el) and el[i]['id'] != s2:
			i += 1
		i += 1

		# Pharmacy address.
		self.data['pharmacy_address'] = '/'
		try:
			self.data['pharmacy_address'] = el[i].text
		except IndexError:
			pass

		# Fax, phone numbers.
		s = 'PhoneSpan'
		el = self.spobj.find_elements('id', s)
		self.data['pharmacy_phone'] = '/'
		if len(el):
			self.data['pharmacy_phone'] = el[0].text

		s = 'FaxSpan'
		el = self.spobj.find_elements('id', s)
		self.data['pharmacy_fax'] = '/'
		if len(el):
			self.data['pharmacy_fax'] = el[0].text

		# Date issued.
		self.is_accredited(True)

		# Pharmacy status
		s = 'FriendlyStatus'
		el = self.spobj.find_elements('id', s)
		self.data['pharmacy_status'] = '/'

		if len(el):
			self.data['pharmacy_status'] = el[0].text

	def fetch_medical_stuff_data(self):
		# Show tab.
		self.show_tab('pharmacy-staff')

		# Get of each stuff member role.
		s = 'div#PharmacyStaffSRLContent > div.ResultDiv > div:nth-child(2) div:nth-child(2)'
		el = self.spobj.find_elements('css', s)

		# Get count of staff in workplace (pharmacy).
		for e in config['RELEVANT_PHARMACY_STAFF']:
			self.data[e] = 0

		for e in el:
			if e.text in self.data:
				self.data[e.text] += 1

		# Be responsible and switch it back :).
		self.show_tab('general')

	def fetch_data(self):
		self.fetch_medical_stuff_data()
		self.fetch_general_data()


class ExcelReader(object):
	def _load_data(self):
		self._wb = load_workbook(self._data_file)
		self._ws = self._wb.active

	def _get_header(self):
		self._data_headers = list()
		row = next(self._ws.rows)
		for cell in row:
			self._data_headers.append(cell.value)

	@classmethod
	def is_generated(cls, item, registration_number):
		t = config['LETTER_FILENAME_TEMPLATE']
		if item == 'label':
			t = config['LABEL_FILENAME_TEMPLATE']

		p = '/'.join([config['INSTANCE_PATH'], config['PDF_ARCHIVE_FOLDER'], t])
		p = p.format(registration_number=registration_number)

		# If file does not exist return the absolute path to directory for next printing.
		if not exists(p):
			n = '/'.join([config['INSTANCE_PATH'], config['PDF_FOLDER'], t])
			n = n.format(registration_number=registration_number)
			return n, False

		return p, True

	@classmethod
	def _extend_letter_data(cls, data):
		# We do this to get correct number of years (in case we decide to run this scraper in some very far future).
		data['current_years_of_practice'] = 0
		try:
			reg_date = dateparser.parse(data['start_date'])
			diff = date.today() - reg_date.date()
			data['current_years_of_practice'] = diff.days / 365
		except:
			pass

		# Pharmacy practice.
		data['pharmacy_practice_years'] = 0
		try:
			date_issued = dateparser.parse(data['date_issued'])
			diff = date.today() - date_issued.date()
			data['pharmacy_practice_years'] = diff.days / 365
		except:
			pass

		data['pharmacy_address_container'] = data['pharmacy_address'].split('\n')
		return data

	def __init__(self, data_file, location):
		self._wb = None
		self._ws = None
		self._data_headers = list()
		self._data_file = data_file
		self._location = location

	def load_file(self):
		self._load_data()
		self._get_header()

	def letter_data(self):
		# Skip header.
		iter_rows = iter(self._ws.rows)
		next(iter_rows)

		for row in iter_rows:
			l = [cell.value for cell in row]
			data = dict(zip(self._data_headers, l))
			if data['location'] != self._location:
				continue
			yield ExcelReader._extend_letter_data(data)


class ExcelWriter(ExcelReader):

	def _create_file(self, fn):
		self._data_file = fn
		if not exists(self._data_file):
			wb = Workbook()
			ws = wb.active
			column_count = 1
			for ct in self._data_headers:
				ws.cell(row=1, column=column_count, value=ct)
				column_count += 1
			wb.save(self._data_file)

	def __init__(self, pl=None, po=None, wo=None):
		super(ExcelWriter, self).__init__(None, None)
		self.obj_pl = pl
		self.obj_po = po
		self.obj_wo = wo
		self._data_headers = list(config['DATA_HEADERS'])
		self._data_headers.extend(config['RELEVANT_PHARMACY_STAFF'])
		self._create_file('/'.join([config['INSTANCE_PATH'], config['EXCEL_OUTPUT_FILENAME']]))
		self._load_data()

	def write(self, data=None):
		if data is None:
			data = dict(self.obj_pl.data)
			data.update(self.obj_po.data)
			data.update(self.obj_wo.data)

		# Data list.
		dl = list()

		for h in self._data_headers:
			v = '/'
			if h in data:
				v = data[h]
			dl.append(v)
		self._ws.append(dl)

	def write_all(self, records):
		for r in records:
			self.write(r)

	def sort(self):
		if self._ws.max_row < 2:
			return False

		self._get_header()
		p = '/'.join([config['INSTANCE_PATH'], config['EXCEL_SORTED_FILENAME']])
		if exists(p):
			remove(p)
		self._create_file(p)

		# Create another ExcelWriter for sorted-data.xlsx file.
		ds = ExcelWriter()
		ds._data_file = p
		ds._load_data()

		records = list()
		next_records = list()

		# Get 'location' of the first record.
		prev_location = None
		iter_rows = iter(self._ws.rows)
		next(iter_rows)
		try:
			second_row = next(iter_rows)
			vl = [cell.value for cell in second_row]
			cd = dict(zip(self._data_headers, vl))
			prev_location = cd['location']
		except StopIteration:
			pass

		# Skip header.
		iter_rows = iter(self._ws.rows)
		next(iter_rows)
		row_count = 1

		for row in iter_rows:
			vl = [cell.value for cell in row]
			cd = dict(zip(self._data_headers, vl))
			if 'registration_number' in cd:
				cd['registration_number'] = int(cd['registration_number'])
			row_count += 1

			save_records = False
			if prev_location == cd['location']:
				records.append(cd)
			elif prev_location != cd['location']:
				next_records.append(cd)
				save_records = True

			if row_count == self._ws.max_row:
				records.append(cd)
				save_records = True

			if save_records:
				# Sort by registration number and _eval required.
				s_records = list()
				while len(records):

					# Evaluate record.
					records[0]['_eval'] = 0
					for f in config['RELEVANT_PHARMACY_STAFF']:
						records[0]['_eval'] += records[0][f]

					s_records.append(records.pop(0))
				s_records.sort(key=itemgetter('registration_number'))

				# Sort by _eval (Pharmacy staff) so we always have busiest pharmacist for specific registration number
				# on first position.
				prev_reg = s_records[0]['registration_number']
				
				while len(s_records):
					if prev_reg == s_records[0]['registration_number']:
						records.append(s_records.pop(0))

					if not len(s_records) or prev_reg != s_records[0]['registration_number']:
						records.sort(key=itemgetter('_eval'), reverse=True)
						ds.write_all(records)
						records = list()
						try:
							prev_reg = s_records[0]['registration_number']
						except IndexError:
							pass

				# Save possible new record(s)
				records = list(next_records)
				next_records = list()
				
				# Begin sorting new location.
				prev_location = cd['location']

		# Save.
		ds.save()
		return True

	def save(self):
		self._wb.save(self._data_file)
