from minsplinter import MinSplinter

from scraper_ocpinfo import load_config, ExcelWriter, PharmacistList, Pharmacist, Workplace, config

# Testing
PHARMACIST_URL = 'http://members.ocpinfo.com/tcpr/public/pr/en/#/forms/show/?table=0x800000000000003D&record=0x000000000000673E&form=PR%20Show%20Person'

WORKPLACE_URL = 'http://members.ocpinfo.com/TCPR/Public/PR/EN/#/forms/show/?table=0x800000000000003C&record=0x0000000000001C1B&form=PR%20Show%20Place'

PHARMACIST_LIST = 'http://members.ocpinfo.com/TCPR/Public/PR/EN/#/forms/new/?table=0x800000000000003D&form=0x800000000000002C&command=0x80000000000007C5&donotreload=ShowSRL1'


def test_excel_write():
	load_config()
	b_obj = MinSplinter(config['NINSPLINTER_CONFIG'])
	pl = PharmacistList('North York', spobj=b_obj)
	p = Pharmacist(PHARMACIST_URL, spobj=b_obj)
	p.fetch_data()
	w = Workplace(WORKPLACE_URL, spobj=b_obj)
	w.fetch_data()
	o = ExcelWriter(pl, p, w)
	o.write()
	o.save()


def test_workplace_fetch():
	load_config()
	b_obj = MinSplinter(config['NINSPLINTER_CONFIG'])
	w = Workplace(WORKPLACE_URL, spobj=b_obj)
	w.fetch_data()
	assert w.data['Pharmacist'] > 0 or w.data['Pharmacy Technician'] > 0


if __name__ == '__main__':
	test_workplace_fetch()
