### About

Main objective: Find independent pharmacies in the Greater Toronto Area that have been open at least 8 years, run by
owners who are getting older or nearing retirement. Send them all a written letter asking if they would be interested
in selling their pharmacy.

### Instalation

I suggest that project gets installed in [virtualenv](https://virtualenv.pypa.io/en/stable/userguide/)
(that is optional).

`pip install -r requirements.txt`

### Additional requirements

Installed desired Web driver (by default firefox).

### Usage

Show help message for script.

`python manage.py --help`

Run scraper for specific location.

`python manage.py run-scraper --location="<Area name>"`

Before we actually generate letters we HAVE TO sort data otherwise letters will not be sent to busiest pharmacist (it
is OK to sort after we have data from all the locations we want).

`python manage.py sort-data`

### Notes

* This project was developed and tested on xUbuntu 16.04 with firefox Web driver.
* Firefox version used in testing: 53.0.3.

### Links

* [Personal website](http://maticzagmajster.ddns.net/)
* [LinkedIn](https://www.linkedin.com/in/matic-zagmajster-525652144/)
* [Upwork](https://www.upwork.com/freelancers/~019f22bbbe739c0369)
